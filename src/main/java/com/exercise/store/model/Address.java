package com.exercise.store.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

import org.hibernate.validator.constraints.NotBlank;

import lombok.Data;

@XmlRootElement
@Data
@Entity
@Table(name = "TB_ADDRESS")
public class Address implements Serializable {

	private static final long serialVersionUID = 1410549876599369979L;

	@Id
	@SequenceGenerator(allocationSize = 0, name = "IdAddressGen", sequenceName = "address_seq")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "IdAddressGen")
	private Long id;

	@NotBlank(message = "O logradouro deve ser informado")
	private String street;

	@NotBlank(message = "O numero deve ser informado")
	private String number;

	@Column(name = "zipcode")
	@NotBlank(message = "O CEP deve ser informado")
	private String zipCode;

	private String district;

	@NotBlank(message = "O municipio deve ser informado")
	private String city;

	@NotBlank(message = "A UF deve ser informada")
	private String state;

	@Column(name = "addressline2")
	private String addressLine2;

}
