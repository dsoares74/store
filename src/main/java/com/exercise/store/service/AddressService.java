package com.exercise.store.service;

import java.util.List;

import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;

import com.exercise.store.exception.NotFoundException;
import com.exercise.store.exception.RestException;
import com.exercise.store.gateway.repository.AddressRepository;
import com.exercise.store.gateway.rest.AddressRestClient;
import com.exercise.store.model.Address;
import com.exercise.store.validation.ModelValidator;
import com.exercise.store.validation.constraints.Zip;

@Service
@Validated
public class AddressService {

	private static final String NUMBER_PATTERN = "[1-9]";
	private final AddressRepository repository;
	private final ModelValidator validator;
	private final AddressRestClient restClient;

	@Autowired
	public AddressService(final AddressRepository repository, final ModelValidator validator, final AddressRestClient restClient) {
		this.repository = repository;
		this.validator = validator;
		this.restClient = restClient;
	}

	public List<Address> findByCep(final @Zip(message = "Cep Invalido") String zipCode) {
		if (NumberUtils.toInt(zipCode) == 0) {
			throw new NotFoundException("Endereco nao encontrado");
		}

		final List<Address> addresses = repository.findByZipCode(zipCode);
		return !addresses.isEmpty() ? addresses : findByCep(replaceLast(zipCode, NUMBER_PATTERN, "0"));
	}

	private String replaceLast(final String text, final String regex, final String replacement) {
		return text.replaceFirst("(?s)" + regex + "(?!.*?" + regex + ")", replacement);
	}

	public Address findById(final Long id) {
		return repository.findOne(id);
	}

	public Address addOrReplace(final Address address) {
		validateAddress(address);
		return repository.save(address);
	}

	private void validateAddress(final Address address) {
		validator.validate(address);

		try {
			restClient.requestAddressByZipCode(address.getZipCode());
		} catch (final RestException e) {
			if (e.getStatusCode() != HttpStatus.NOT_FOUND) {
				throw e;
			}
		}
	}

	public void remove(final Long id) {
		repository.delete(id);
	}

}