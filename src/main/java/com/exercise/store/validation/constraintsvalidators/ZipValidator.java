package com.exercise.store.validation.constraintsvalidators;

import static java.util.Arrays.stream;

import java.util.List;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import com.exercise.store.validation.constraints.Zip;

public class ZipValidator implements ConstraintValidator<Zip, String> {

	private static final String[] BRAZIL_ZIP_PATTERNS = {"^([01][1-9][0-9]{6})$", "^(((?!6)[2-9])[0-9]{7})$", "^(6((?!7)[0-9])[0-9]{6})$"};
	private static final String[] INVALID_ZIP_PATTERNS = {"^(?:(?!1{8}).)*$", "^(?:(?!2{8}).)*$", "^(?:(?!3{8}).)*$", "^(?:(?!4{8}).)*$", "^(?:(?!5{8}).)*$", "^(?:(?!6{8}).)*$", "^(?:(?!7{8}).)*$", "^(?:(?!8{8}).)*$", "^(?:(?!9{8}).)*$"};

	private List<Pattern> invalidPatterns;
	private List<Pattern> validPatterns;

	@Override
	public void initialize(final Zip zipValue) {
		validPatterns = stream(BRAZIL_ZIP_PATTERNS).map(Pattern::compile).collect(Collectors.toList());
		invalidPatterns = stream(INVALID_ZIP_PATTERNS).map(Pattern::compile).collect(Collectors.toList());
	}

	@Override
	public boolean isValid(final String value, final ConstraintValidatorContext context) {
		if (value == null) {
			return true;
		}

		return invalidPatterns.stream().allMatch(p -> p.matcher(value).matches()) && validPatterns.stream().anyMatch(p -> p.matcher(value).matches());
	}

}
