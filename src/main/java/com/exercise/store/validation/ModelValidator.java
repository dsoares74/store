package com.exercise.store.validation;

import static org.apache.commons.collections.CollectionUtils.isNotEmpty;

import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.Validator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ModelValidator {

	private final Validator validator;

	@Autowired
	public ModelValidator(final Validator validator) {
		this.validator = validator;
	}

	public <T> void validate(final T model) {
		final Set<ConstraintViolation<T>> violations = validator.validate(model);
		if (isNotEmpty(violations)) {
			throw new ConstraintViolationException(violations);
		}
	}

}
