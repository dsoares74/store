package com.exercise.store.web.controller;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.web.bind.annotation.RequestMethod.DELETE;
import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;
import static org.springframework.web.bind.annotation.RequestMethod.PUT;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.exercise.store.model.Address;
import com.exercise.store.service.AddressService;

@RestController
@RequestMapping("/endereco")
@Validated
public class AddressController {

	private final AddressService service;

	@Autowired
	public AddressController(final AddressService service) {
		this.service = service;
	}

	@RequestMapping(value = "/cep/{cep}", method = GET, produces = { APPLICATION_JSON_VALUE })
	public List<Address> findByZip(@PathVariable("cep") final String zipCode) {
		return service.findByCep(zipCode);
	}

	@RequestMapping(value = "/{id}", method = GET, produces = { APPLICATION_JSON_VALUE })
	public Address getEndereco(@PathVariable("id") final Long idEndereco) {
		return service.findById(idEndereco);
	}

	@RequestMapping(method = POST, produces = { APPLICATION_JSON_VALUE }, consumes = { APPLICATION_JSON_VALUE })
	public Address addEndereco(final @RequestBody @Valid Address address) {
		return service.addOrReplace(address);
	}

	@RequestMapping(method = PUT, produces = { APPLICATION_JSON_VALUE }, consumes = { APPLICATION_JSON_VALUE })
	public Address updateEndereco(final @RequestBody @Valid Address address) {
		return service.addOrReplace(address);
	}

	@RequestMapping(value = "/{id}", method = DELETE, produces = { APPLICATION_JSON_VALUE })
	public void remover(@PathVariable("id") final Long idEndereco) {
		service.remove(idEndereco);
	}

}