package com.exercise.store.web.controller;

import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

import javax.validation.ConstraintViolationException;

import lombok.extern.slf4j.Slf4j;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.HttpMediaTypeNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.exercise.store.exception.NotFoundException;
import com.exercise.store.exception.RestException;

@Slf4j
@ControllerAdvice
public class ExceptionHandlerController {

	@ExceptionHandler(NotFoundException.class)
	@ResponseStatus(HttpStatus.NOT_FOUND)
	@ResponseBody
	public Map<String, String> handleNotFoundException(final NotFoundException e) {
		log.error("[The resource requested was not found] ", e);
		return getMap(e.getMessage());
	}

	@ExceptionHandler(ConstraintViolationException.class)
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	@ResponseBody
	public Map<String, String> handleValidationException(final ConstraintViolationException e) {
		final String msg = e.getConstraintViolations().stream().map(c -> c.getMessage()).collect(Collectors.joining("; "));
		log.error("[The resource is invalid] {}", msg);
		return getMap(msg);
	}

	@ExceptionHandler(MethodArgumentNotValidException.class)
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	@ResponseBody
	public Map<String, String> processValidationError(final MethodArgumentNotValidException ex) {
		final String msg = ex.getBindingResult().getFieldErrors().stream().map(f -> f.getDefaultMessage()).collect(Collectors.joining("; "));
		log.info("Argument: [{}] is not valid, see: {}", ex.getBindingResult().getTarget(), msg);
		return getMap(msg);
	}

	@ExceptionHandler(HttpMessageNotReadableException.class)
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	@ResponseBody
	public Map<String, String> handleHttpMessageNotReadableException(final HttpMessageNotReadableException e) {
		final String message = "Invalid Json data.";
		return getMap(message);
	}

	@ExceptionHandler(HttpMediaTypeNotSupportedException.class)
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	@ResponseBody
	public Map<String, String> handleHttpMediaTypeNotSupportedException(final HttpMediaTypeNotSupportedException e) {
		final String message = "Unsupported content type: "+ e.getContentType();
		return getMap(message);
	}

	@SuppressWarnings("rawtypes")
	@ExceptionHandler(RestException.class)
	@ResponseBody
	public ResponseEntity handleRestException(final RestException e) {
		final String message = "Some error between the gateway and upstream server happened: " + e.getMessage();
		log.error(message, e);
		return ResponseEntity.status(e.getStatusCode()).body(getMap(e.getMessage()));
	}

	private Map<String, String> getMap(final String errorMessage) {
		final Map<String, String> map = new HashMap<>();
		map.put("error", errorMessage);
		return map;
	}
}
