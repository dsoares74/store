package com.exercise.store.gateway.rest;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

import lombok.extern.slf4j.Slf4j;

import org.springframework.http.client.ClientHttpResponse;
import org.springframework.util.StreamUtils;
import org.springframework.web.client.DefaultResponseErrorHandler;
import org.springframework.web.client.ResponseErrorHandler;

import com.exercise.store.exception.RestException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

@Slf4j
public class RestClientErrorHandler implements ResponseErrorHandler {

	private ResponseErrorHandler defaultErrorHandler = new DefaultResponseErrorHandler();

	@Override
	public void handleError(final ClientHttpResponse response) throws IOException {
		final String errorMessage = StreamUtils.copyToString(response.getBody(), Charset.defaultCharset());
		throw new RestException(response.getStatusCode(), treatErrorMessage(errorMessage));
	}

	private String treatErrorMessage(final String json) {
		final ObjectMapper mapper = new ObjectMapper();
		Map<String, String> map = new HashMap<String, String>();

		try {
			map = mapper.readValue(json, new TypeReference<Map<String, String>>(){});
		} catch (final IOException e) {
			log.error("Error to converter the message", e);
		}

		return map.values().stream().collect(Collectors.joining("; "));
	}

	@Override
	public boolean hasError(final ClientHttpResponse response) throws IOException {
		return defaultErrorHandler.hasError(response);
	}

}
