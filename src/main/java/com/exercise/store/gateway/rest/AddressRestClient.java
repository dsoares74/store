package com.exercise.store.gateway.rest;

import java.util.List;

import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.exercise.store.model.Address;

@Component
public class AddressRestClient {

	private static final String ENDPOINT = "http://localhost:8080/endereco/cep/{cep}";
	private final RestTemplate restTemplate;

	public AddressRestClient(final RestTemplateBuilder restTemplateBuilder) {
		this.restTemplate = restTemplateBuilder.errorHandler(new RestClientErrorHandler()).build();
	}

	public List<Address> requestAddressByZipCode(final String zip) {
		final ResponseEntity<List<Address>> response = this.restTemplate.exchange(ENDPOINT, HttpMethod.GET, createHttpEntity(), createResponseType(), zip);
		final List<Address> addresses = response.getBody();
		return addresses;
	}

	private HttpEntity<String> createHttpEntity() {
		final HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		final HttpEntity<String> httpEntity = new HttpEntity<>(headers);
		return httpEntity;
	}

	private ParameterizedTypeReference<List<Address>> createResponseType() {
		return new ParameterizedTypeReference<List<Address>>() {};
	}
}
