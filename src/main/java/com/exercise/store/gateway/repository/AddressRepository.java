package com.exercise.store.gateway.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.exercise.store.model.Address;

public interface AddressRepository extends CrudRepository<Address, Long> {

	List<Address> findByZipCode(String zipCode);

}
