package com.exercise.store.exception;

import org.springframework.http.HttpStatus;

public class RestException extends RuntimeException {

	private static final long serialVersionUID = 1189219700314907429L;

	private final HttpStatus statusCode;

	public RestException(final HttpStatus statusCode, final String message) {
		super(message);
		this.statusCode = statusCode;
	}

	public HttpStatus getStatusCode() {
		return this.statusCode;
	}

}
