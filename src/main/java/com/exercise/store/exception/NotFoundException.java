package com.exercise.store.exception;

import static org.apache.commons.lang3.StringUtils.defaultIfBlank;

public class NotFoundException extends RuntimeException {

	private static final long serialVersionUID = 8459169980376768563L;
	private static final String RESOURCE_NOT_FOUND = "Resource not found.";

	public NotFoundException() {
		super(RESOURCE_NOT_FOUND);
	}

	public NotFoundException(final String message) {
		super(defaultIfBlank(message, RESOURCE_NOT_FOUND));
	}

}
