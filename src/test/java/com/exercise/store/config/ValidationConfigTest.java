package com.exercise.store.config;

import javax.validation.Validator;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;

@Configuration
@ComponentScan(basePackages = { "com.exercise.store.validation" })
public class ValidationConfigTest {

	@Bean
	public Validator validator() {
		return new LocalValidatorFactoryBean();
	}

}