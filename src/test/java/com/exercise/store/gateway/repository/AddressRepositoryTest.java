package com.exercise.store.gateway.repository;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import com.exercise.store.model.Address;
import com.exercise.store.util.AddressGenerator;

@RunWith(SpringRunner.class)
@DataJpaTest
public class AddressRepositoryTest {

	@Autowired
	private TestEntityManager entityManager;

	@Autowired
	private AddressRepository repository;

	@Test
	public void testFindByCepInvalidNumber() {
		final List<Address> address = this.repository.findByZipCode("00000000");
		assertThat(address).isEmpty();
	}

	@Test
	public void testFindByCep() {
		final Address entity = AddressGenerator.create();
		this.entityManager.persist(entity);
		final List<Address> addresses = this.repository.findByZipCode(AddressGenerator.ZIP_CODE);
		assertThat(addresses).isNotEmpty();
		assertThat(addresses.size()).isEqualTo(1);

		final Address address = addresses.get(0);
		assertThat(address.getDistrict()).isEqualTo(entity.getDistrict());
		assertThat(address.getStreet()).isEqualTo(entity.getStreet());
		assertThat(address.getCity()).isEqualTo(entity.getCity());
		assertThat(address.getNumber()).isEqualTo(entity.getNumber());
		assertThat(address.getState()).isEqualTo(entity.getState());
		assertThat(address.getId()).isNotNull();
		assertThat(address.getAddressLine2()).isNull();
	}
}
