package com.exercise.store.validation;

import javax.validation.ConstraintViolationException;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.exercise.store.config.ValidationConfigTest;
import com.exercise.store.model.Address;
import com.exercise.store.util.AddressGenerator;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { ValidationConfigTest.class })
public class ModelValidatorTest {

	@Autowired
	private ModelValidator modelValidator;

	@Test(expected = IllegalArgumentException.class)
	public final void test_NullModel_shouldReturnIllegalArgument() {
		modelValidator.validate(null);
	}

	@Test(expected = ConstraintViolationException.class)
	public void test_invalidModel_shouldReturnConstraintViolationException() {
		final Address address = new Address();
		modelValidator.validate(address);
	}

	@Test
	public void testValidModel_shouldReturnSuccess() {
		final Address address = AddressGenerator.create();
		modelValidator.validate(address);
	}
}

