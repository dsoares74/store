package com.exercise.store.validation.constraint;

import static java.util.Arrays.stream;
import static org.junit.Assert.assertTrue;

import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

import lombok.Data;

import org.apache.commons.collections.CollectionUtils;
import org.junit.BeforeClass;
import org.junit.Test;

import com.exercise.store.validation.constraints.Zip;

public class ZipTest {

	private Model test;

	private static Validator validator;

	@BeforeClass
	public static void setUp() throws Exception {
		final ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
		validator = factory.getValidator();
	}

	@Test
	public final void testValidZip() {
		test = new Model("02238110");
		assertTrue(validator.validate(test).isEmpty());
	}

	@Test
	public final void testInvalidZips() {
		final Model[] invalidZips = { new Model("00000000"), new Model("13456"), new Model("123456789")};
		stream(invalidZips).forEach(this::checkInvalidZips);

	}

	@Test
	public final void testFalsePositive() {
		final Model[] falsePositives = {
				new Model("11111111"), new Model("22222222"),
				new Model("33333333"), new Model("44444444"),
				new Model("55555555"), new Model("66666666"),
				new Model("77777777"), new Model("88888888"),
				new Model("99999999") };
		stream(falsePositives).forEach(this::checkInvalidZips);
	}

	public void checkInvalidZips(final Model model) {
		final Set<ConstraintViolation<Model>> constraintViolations = validator.validate(model);
		assertTrue(CollectionUtils.isNotEmpty(constraintViolations));
		assertTrue(constraintViolations.stream().anyMatch(c -> "invalid zip code".equalsIgnoreCase(c.getMessage())));
	}

	@Data
	private class Model {

		@Zip(message = "invalid zip code")
		private String zipCode;

		Model(final String zipCode) {
			this.zipCode = zipCode;
		}
	}
}
