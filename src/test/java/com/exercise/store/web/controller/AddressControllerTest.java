package com.exercise.store.web.controller;

import static com.exercise.store.util.AddressGenerator.ZIP_CODE;
import static java.util.Arrays.asList;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.hamcrest.core.IsNull.nullValue;
import static org.hamcrest.core.StringContains.containsString;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.exercise.store.model.Address;
import com.exercise.store.service.AddressService;
import com.exercise.store.util.AddressGenerator;

@RunWith(SpringRunner.class)
@WebMvcTest(AddressController.class)
public class AddressControllerTest {

	private static final Long ID_ADDRESS = 1L;

	private Address address;

	@Autowired
	private MockMvc mvc;

	@MockBean
	private AddressService service;

	@Before
	public void setUp() throws Exception {
		address = AddressGenerator.create();
		given(service.findByCep(ZIP_CODE)).willReturn(asList(address));
		given(service.findById(ID_ADDRESS)).willReturn(address);
		given(service.addOrReplace(address)).will(invocation -> {
			final Address ad = invocation.getArgumentAt(0, Address.class);
			if (ad.getId() == null) {
				ad.setId(2L);
			} else {
				ad.setAddressLine2("addressLine2");
			}
			return ad;
		});
	}

	@Test
	public final void testListarEnderecoPorCep() throws Exception {
		this.mvc.perform(
			get("/endereco/cep/{cep}", ZIP_CODE).accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$[0].district", containsString("bairro")))
				.andExpect(jsonPath("$[0].street", equalTo("logradouro")))
				.andExpect(jsonPath("$[0].number", containsString("sem numero")))
				.andExpect(jsonPath("$[0].city", is("municipio")))
				.andExpect(jsonPath("$[0].state", is("uf")))
				.andExpect(jsonPath("$[0].addressLine2", nullValue()))
				.andExpect(jsonPath("$[0].zipCode", equalTo(ZIP_CODE)));
	}

	@Test
	public final void testGetEndereco() throws Exception {
		this.mvc.perform(get("/endereco/{id}", ID_ADDRESS).accept(MediaType.APPLICATION_JSON))
		.andExpect(status().isOk()).andExpect(jsonPath("$.district", equalTo("bairro")))
		.andExpect(jsonPath("$.street", equalTo("logradouro")))
		.andExpect(jsonPath("$.number", equalTo("sem numero")))
		.andExpect(jsonPath("$.city", equalTo("municipio"))).andExpect(jsonPath("$.state", equalTo("uf")))
		.andExpect(jsonPath("$.addressLine2", nullValue())).andExpect(jsonPath("$.zipCode", equalTo(ZIP_CODE)));
	}

	@Test
	public final void testAddEndereco() throws Exception {
		final String json = AddressGenerator.createJson(address);
		this.mvc.perform(post("/endereco").content(json).contentType(MediaType.APPLICATION_JSON))
			.andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
			.andExpect(jsonPath("$.id", equalTo(2)))
			.andExpect(jsonPath("$.addressLine2", nullValue()));

	}

	@Test
	public final void testUpdateEndereco() throws Exception {
		address.setId(ID_ADDRESS);
		final String json = AddressGenerator.createJson(address);
		this.mvc.perform(put("/endereco").content(json).contentType(MediaType.APPLICATION_JSON))
			.andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
			.andExpect(jsonPath("$.id", equalTo(ID_ADDRESS.intValue())))
			.andExpect(jsonPath("$.addressLine2", containsString("addressLine2")));
	}

	@Test
	public final void testRemover() throws Exception {
		this.mvc.perform(delete("/endereco/{id}", ID_ADDRESS).accept(MediaType.APPLICATION_JSON))
		.andExpect(status().isOk());
	}

}
