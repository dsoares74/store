package com.exercise.store.util;

import com.exercise.store.model.Address;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public final class AddressGenerator {
	public static final String ZIP_CODE = "05489012";

	private AddressGenerator() {
		super();
	}

	public static String createJson(final Address a) {
		final ObjectMapper mapper = new ObjectMapper();
		String json = null;

		try {
			json = mapper.writeValueAsString(a);
		} catch (final JsonProcessingException e) {
			e.printStackTrace();
		}

		return json;
	}

	public static Address create() {
		final Address e = new Address();
		e.setDistrict("bairro");
		e.setStreet("logradouro");
		e.setCity("municipio");
		e.setNumber("sem numero");
		e.setState("uf");
		e.setZipCode(ZIP_CODE);
		return e;
	}
}
