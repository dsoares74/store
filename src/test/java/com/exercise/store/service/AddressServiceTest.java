package com.exercise.store.service;

import static com.exercise.store.util.AddressGenerator.ZIP_CODE;
import static com.exercise.store.util.AddressGenerator.create;
import static java.util.Arrays.asList;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.willThrow;
import static org.mockito.Mockito.verify;

import java.util.List;

import javax.validation.ConstraintViolationException;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.junit4.SpringRunner;

import com.exercise.store.exception.NotFoundException;
import com.exercise.store.exception.RestException;
import com.exercise.store.gateway.repository.AddressRepository;
import com.exercise.store.gateway.rest.AddressRestClient;
import com.exercise.store.model.Address;
import com.exercise.store.validation.ModelValidator;

@RunWith(SpringRunner.class)
@SpringBootTest
public class AddressServiceTest {

	private static final String VALID_ZIP_CODE = "05400000";

	private static final String INVALID_ZIP_CODE = "22222222";

	private static final String NOT_FOUND_ZIP_CODE = "02224702";

	private static final Long ID_ADDRESS = 1L;

	private Address address;

	@Autowired
	private AddressService service;

	@MockBean
	private ModelValidator validator;

	@MockBean
	private AddressRepository repository;

	@MockBean
	private AddressRestClient restClient;

	@Before
	public void setUp() throws Exception {
		address = create();
		given(repository.findByZipCode(VALID_ZIP_CODE)).willReturn(asList(address));
		given(repository.findOne(ID_ADDRESS)).willReturn(address);
		given(restClient.requestAddressByZipCode(ZIP_CODE)).willThrow(new RestException(HttpStatus.NOT_FOUND, "Address not found"));
		given(restClient.requestAddressByZipCode(INVALID_ZIP_CODE)).willThrow(new RestException(HttpStatus.BAD_REQUEST, "Invalid zip code"));
	}

	@Test
	public final void testFindByValidCep_shouldReturnAddresses() {
		final List<Address> adds = service.findByCep(ZIP_CODE);
		assertFalse(adds.isEmpty());
	}

	@Test(expected = NotFoundException.class)
	public final void testFindByInexistentCep_shouldThrowException() {
		service.findByCep(NOT_FOUND_ZIP_CODE);
	}

	@Test(expected = ConstraintViolationException.class)
	public final void testFindByInvalidCep_shouldThrowException() {
		service.findByCep(INVALID_ZIP_CODE);
	}

	@Test
	public final void testFindById() {
		final Address address = service.findById(1l);
		assertNotNull(address);
	}

	@Test
	public final void testFindById_InvalidID_shouldReturnEmptyAddress() {
		final Address address = service.findById(0l);
		assertNull(address);
	}

	@Test
	public final void testAddOrReplace_NotFoundAddres_shouldReturnSuccess() {
		service.addOrReplace(address);
		verify(repository).save(address);
	}

	@Test(expected = RestException.class)
	public final void testAddOrReplace_InvalidZipCode_shouldThrowException() {
		address.setZipCode(INVALID_ZIP_CODE);
		service.addOrReplace(address);
	}

	@Test(expected = ConstraintViolationException.class)
	public final void testAddOrReplace_InvalidAddress_shouldThrowException() {
		willThrow(ConstraintViolationException.class).given(validator).validate(address);
		service.addOrReplace(address);
	}

	@Test
	public final void testRemove() {
		service.remove(ID_ADDRESS);
		verify(repository).delete(ID_ADDRESS);
	}

}
