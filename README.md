# EXERCISES 1 e 2 #

* Create a Find ZipCode service 
* Salve the user's address using CRUD rules (Create, Read, Update e Delete). 

# Unit Tests

* We are using JUnit for unit tests.

* To run, execute the following steps in terminal:

```sh
$ mvn test
```

### How do I get set up? ###

* Dependencies
* To get the Dependencies, simply run the following command:

```sh
$ mvn clean package
```

To start the application, simply run the following command:

```sh
$ java -jar target/store-0.0.1-SNAPSHOT.jar
```

Or

```sh
$ mvn spring-boot:run
```

![alt tag](/src/main/resources/VM_Config.png)


# H2 Console

* After starting the application, to access the database console digit this URL in your web browser:
http://localhost:8080/console

* This is the login console page, check the JDBC URL and click in Connect button

![alt tag](/H2ConsoleLogin.png)

* After that, you can query the recorded addresses' info

![alt tag](/H2ConsoleConsulta.png)

